#ifndef RESOURCEMANAGER
#define RESOURCEMANAGER

#include <map>
#include <string>

#include <glad/glad.h>

#include "cTexture2D.h"
#include "cShader.h"

class cResourceManager
{
public:
	static std::map< std::string, cShader > Shaders;
	static std::map< std::string, cTexture2D > Textures;

	static cShader LoadShader( const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile, std::string name );
	static cShader GetShader( std::string name );

	static cTexture2D LoadTexture( const GLchar* file, GLboolean alpha, std::string name );
	static cTexture2D GetTexture( std::string name );

	static void Clear( );

private:
	cResourceManager( ) { };
	static cShader loadShaderFromFile( const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile = nullptr );
	static cTexture2D loadTextureFromFile( const GLchar* file, GLboolean alpha );
};
#endif
