#ifndef BALLOBJECT
#define BALLOBJECT

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "cTexture2D.h"
#include "cSpriteRenderer.h"
#include "cGameObject.h"

class cBallObject : public cGameObject
{
public:
	GLfloat Radius;
	GLboolean Stuck;

	cBallObject( );
	cBallObject( glm::vec2 pos, GLfloat radius, glm::vec2 velocity, cTexture2D sprite );

	glm::vec2 Move( GLfloat dt, GLuint windowWidth );

	void Reset( glm::vec2 position, glm::vec2 velocity );
};
#endif // !BALLOBJECT
