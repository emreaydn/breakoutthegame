#include "cBallObject.h"

cBallObject::cBallObject( )
{
	this->Radius = 12.5f;
	this->Stuck = true;
}

cBallObject::cBallObject( glm::vec2 pos, GLfloat radius, glm::vec2 velocity, cTexture2D sprite )
{
	this->Position = pos;
	this->Size = glm::vec2( radius * 2 );
	this->Sprite = sprite;
	this->Color = glm::vec3( 1.0f );
	this->Velocity = velocity;
	this->Stuck = true;

	this->Radius = radius;
}

glm::vec2 cBallObject::Move( GLfloat dt, GLuint windowWidth )
{
	if( !this->Stuck )
	{
		this->Position += this->Velocity * dt;
		if( this->Position.x <= 0.0f )
		{
			this->Velocity.x = -this->Velocity.x;
			this->Position.x = 0.0f;
		}
		else if( this->Position.x + this->Size.x >= windowWidth )
		{
			this->Velocity.x = -this->Velocity.x;
			this->Position.x = windowWidth - this->Size.x;
		}
		if( this->Position.y <= 0.0f )
		{
			this->Velocity.y = -this->Velocity.y;
			this->Position.y = 0.0f;
		}
	}
	return this->Position;
}

void cBallObject::Reset( glm::vec2 position, glm::vec2 velocity )
{
	this->Position = position;
	this->Velocity = velocity;
	this->Stuck = true;
}
