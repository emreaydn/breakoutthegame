#ifndef SPRITERENDERER
#define SPRITERENDERER

#include "cShader.h"
#include "cTexture2D.h"

class cSpriteRenderer
{
public:
	cSpriteRenderer( cShader &shader );
	~cSpriteRenderer( );

	void DrawSprite( cTexture2D &texture, glm::vec2 position, glm::vec2 size = glm::vec2( 10, 10 ),
		GLfloat rotate = 0.0f, glm::vec3 color = glm::vec3( 1.0f ) );
private:
	cShader Shader;
	GLuint quadVAO;

	void initRenderData( );
};
#endif
