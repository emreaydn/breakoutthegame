#ifndef GAMELEVEL
#define GAMELEVEL

#include <vector>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "cGameObject.h"
#include "cSpriteRenderer.h"
#include "cResourceManager.h"

class cGameLevel
{
public:
	std::vector< cGameObject > Bricks;

	cGameLevel( );

	void Load( const GLchar* file, GLuint levelWidth, GLuint levelHeight );
	void Draw( cSpriteRenderer &renderer );
	
	GLboolean IsCompleted( );
private:
	void init( std::vector< std::vector< GLuint > > tileData, GLuint levelWidth, GLuint levelHeight );
};
#endif // !GAMELEVEL
