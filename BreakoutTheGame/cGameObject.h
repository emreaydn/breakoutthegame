#ifndef GAMEOBJECT
#define GAMEOBJECT

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "cTexture2D.h"
#include "cSpriteRenderer.h"

class cGameObject
{
public:
	glm::vec2 Position, Size, Velocity;
	glm::vec3 Color;
	GLfloat Rotation;
	GLboolean IsSolid;
	GLboolean Destroyed;

	cTexture2D Sprite;

	cGameObject( );
	cGameObject( glm::vec2 pos, glm::vec2 size, cTexture2D sprite, glm::vec3 color = glm::vec3( 1.0f ), glm::vec2 velocity = glm::vec2( 0.0f, 0.0f ) );

	virtual void Draw( cSpriteRenderer &renderer );
};
#endif // !GAMEOBJECT
