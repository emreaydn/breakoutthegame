#include "cGame.h"
#include "cResourceManager.h"
#include "cSpriteRenderer.h"
#include "cBallObject.h"

cSpriteRenderer* Renderer;

const glm::vec2 PlayerSize( 100, 20 );
const GLfloat PlayerVelocity( 500.0f );
cGameObject* Player;

const glm::vec2 InitialVelocity( 100.0f, -350.0f );
const GLfloat BallRadius = 12.5f;
cBallObject* Ball;

GLboolean CheckCollision( cGameObject &one, cGameObject &two );

cGame::cGame( GLuint Width, GLuint Height ) : Keys( )
{
	this->State = GameState::GAME_ACTIVE;
	this->Width = Width;
	this->Height = Height;
	return;
}

cGame::~cGame( )
{
	delete Renderer;
}

void cGame::Init( )
{
	cResourceManager::LoadShader( "assets/shaders/vertex.glsl", "assets/shaders/frag.glsl", nullptr, "sprite" );

	glm::mat4 projection = glm::ortho( 0.0f, static_cast< GLfloat >( this->Width ), static_cast< GLfloat >( this->Height ), 0.0f, -1.0f, 1.0f );
	cResourceManager::GetShader( "sprite" ).Use( ).SetInteger( "image", 0 );
	cResourceManager::GetShader( "sprite" ).SetMatrix4( "projection", projection );

	cResourceManager::LoadTexture( "assets/textures/awesomeface.png", GL_TRUE, "face" );
	cResourceManager::LoadTexture( "assets/textures/block.png", GL_TRUE, "block" );
	cResourceManager::LoadTexture( "assets/textures/block_solid.png", GL_TRUE, "block_solid" );
	cResourceManager::LoadTexture( "assets/textures/background.jpg", GL_TRUE, "background" );
	cResourceManager::LoadTexture( "assets/textures/paddle.png", GL_TRUE, "paddle" );

	cGameLevel one, two, three, four;
	one.Load( "assets/levels/one.lvl", this->Width, this->Height * 0.5f );
	two.Load( "assets/levels/two.lvl", this->Width, this->Height * 0.5f );
	three.Load( "assets/levels/three.lvl", this->Width, this->Height * 0.5f );
	four.Load( "assets/levels/four.lvl", this->Width, this->Height * 0.5f );	

	this->Levels.push_back( one );
	this->Levels.push_back( two );
	this->Levels.push_back( three );
	this->Levels.push_back( four );

	this->Level = 0;

	glm::vec2 playerPos = glm::vec2( this->Width / 2 - PlayerSize.x / 2, this->Height - PlayerSize.y );
	Player = new cGameObject( playerPos, PlayerSize, cResourceManager::GetTexture( "paddle" ) );

	glm::vec2 ballPos = playerPos + glm::vec2( PlayerSize.x / 2 - BallRadius, -BallRadius * 2 );
	Ball = new cBallObject( ballPos, BallRadius, InitialVelocity, cResourceManager::GetTexture( "face" ) );

	cShader tempShader;
	tempShader = cResourceManager::GetShader( "sprite" );
	Renderer = new cSpriteRenderer( tempShader );
}

void cGame::ProcessInput( GLfloat dt )
{
	if( this->State == GAME_ACTIVE )
	{
		GLfloat velocity = PlayerVelocity * dt;

		if( this->Keys[ GLFW_KEY_A ] )
		{
			if( Player->Position.x >= 0 )
			{
				Player->Position.x -= velocity;
			}

		}
		if( this->Keys[ GLFW_KEY_D ] )
		{
			if( Player->Position.x <= this->Width - PlayerSize.x )
			{
				Player->Position.x += velocity;
			}
		}
		if( this->Keys[ GLFW_KEY_SPACE ] )
		{
			Ball->Stuck = false;
		}
	}
}

void cGame::Update( GLfloat dt )
{
	Ball->Move( dt, this->Width );

}

void cGame::Render( )
{
	if( this->State == GAME_ACTIVE )
	{
		Renderer->DrawSprite( cResourceManager::GetTexture( "background" ), glm::vec2( 0.0f, 0.0f ), glm::vec2( this->Width, this->Height ), 0.0f );
		this->Levels[ this->Level ].Draw( *Renderer );
		Player->Draw( *Renderer );
		Ball->Draw( *Renderer );
	}
	
}

GLboolean CheckCollision( cGameObject &one, cGameObject &two )
{
}