#include "cTexture2D.h"

#include <iostream>

cTexture2D::cTexture2D( )
{
	this->Width = 0;
	this->Height = 0;
	this->InternalFormat = GL_RGB;
	this->ImageFormat = GL_RGB;
	this->WrapS = GL_REPEAT;
	this->WrapT = GL_REPEAT;
	this->FilterMin = GL_LINEAR;
	this->FilterMax = GL_LINEAR;

	glGenTextures( 1, &this->ID );
	return;
}

void cTexture2D::Generate( GLuint Width, GLuint Height, unsigned char* data )
 {
	this->Width = Width;
	this->Height = Height;

	glBindTexture( GL_TEXTURE_2D, this->ID );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->WrapS );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->WrapT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->FilterMin );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->FilterMax );

	glTexImage2D( GL_TEXTURE_2D, 0, this->InternalFormat, Width, Height, 0, this->ImageFormat, GL_UNSIGNED_BYTE, data );
	glGenerateMipmap( GL_TEXTURE_2D );

	glBindTexture( GL_TEXTURE_2D, 0 );
}

void cTexture2D::Bind( ) const
{
	glBindTexture( GL_TEXTURE_2D, this->ID );
}

