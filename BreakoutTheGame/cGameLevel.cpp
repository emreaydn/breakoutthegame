#include "cGameLevel.h"

#include <fstream>
#include <sstream>

cGameLevel::cGameLevel( )
{
	return;
}

void cGameLevel::Load( const GLchar* file, GLuint levelWidth, GLuint levelHeight )
{
	this->Bricks.clear( );
	GLuint tileCode;
	cGameLevel level;
	std::string line;
	std::ifstream fstream( file );
	std::vector< std::vector< GLuint > > tileData;
	
	if( fstream )
	{
		while( std::getline( fstream, line ) )
		{
			std::istringstream sstream( line );
			std::vector< GLuint > row;
			while( sstream >> tileCode )
				row.push_back( tileCode );
			tileData.push_back( row );
		}
		if( tileData.size( ) > 0 )
		{
			this->init( tileData, levelWidth, levelHeight );
		}
	}
}

void cGameLevel::Draw( cSpriteRenderer &renderer )
{
	for( int i = 0; i < this->Bricks.size( ); i++ )
	{
		cGameObject curObj = this->Bricks[ i ];
		if( !curObj.Destroyed )
		{
			curObj.Draw( renderer);
		}
	}
}

GLboolean cGameLevel::IsCompleted( )
{
	for( cGameObject &tile : this->Bricks )
	{
		if( !tile.IsSolid && !tile.Destroyed )
		{
			return GL_FALSE;
		}
	}
	return GL_TRUE;
}

void cGameLevel::init( std::vector< std::vector< GLuint > > tileData, GLuint levelWidth, GLuint levelHeight )
{
	GLuint height = tileData.size( );
	GLuint width = tileData[ 0 ].size( );
	GLfloat unitWidth = levelWidth / static_cast< GLfloat >( width );
	GLfloat unitHeight = levelHeight / height;

	for( GLuint y = 0; y < height; ++y )
	{
		for( GLuint x = 0; x < width; ++x )
		{
			if( tileData[ y ][ x ] == 1 )
			{
				glm::vec2 pos( unitWidth * x, unitHeight * y );
				glm::vec2 size( unitWidth, unitHeight );
				cGameObject curObj( pos, size, cResourceManager::GetTexture( "block_solid" ), glm::vec3( 0.8f, 0.8f, 0.8f ) );
				curObj.IsSolid = true;
				this->Bricks.push_back( curObj );
			}
			else if( tileData[ y ][ x ] > 1 )
			{
				glm::vec3 color = glm::vec3( 1.0f );
				if( tileData[ y ][ x ] == 2 )
					color = glm::vec3( 0.2f, 0.6f, 1.0f );
				if( tileData[ y ][ x ] == 3 )
					color = glm::vec3( 0.0f, 0.7f, 1.0f );
				if( tileData[ y ][ x ] == 4 )
					color = glm::vec3( 0.8f, 0.8f, 0.4f );
				if( tileData[ y ][ x ] == 5 )
					color = glm::vec3( 1.0f, 0.6f, 0.0f );

				glm::vec2 pos( unitWidth * x, unitHeight * y );
				glm::vec2 size( unitWidth, unitHeight );
				this->Bricks.push_back( cGameObject( pos, size, cResourceManager::GetTexture( "block" ), color ) );
			}
		}
	}
}

