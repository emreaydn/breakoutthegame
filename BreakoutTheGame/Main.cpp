#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "cGame.h"
#include "cResourceManager.h"

void key_callback( GLFWwindow* window, int key, int scancode, int action, int mode );

int sWidth = 800;
int sHeight = 600;

cGame BreakoutGame( sWidth, sHeight );

int main( )
{
	glfwInit( );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	//glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
	glfwWindowHint( GLFW_RESIZABLE, GL_FALSE );

	GLFWwindow* window = glfwCreateWindow( 1024, 768, "Breakout", nullptr, nullptr );
	glfwGetFramebufferSize( window, &sWidth, &sHeight );
	glfwMakeContextCurrent( window );
	gladLoadGLLoader( ( GLADloadproc ) glfwGetProcAddress );
	glViewport( 0, 0, sWidth, sHeight );

	glfwSetKeyCallback( window, key_callback );
	
	glEnable( GL_CULL_FACE );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	BreakoutGame.Init( );
	BreakoutGame.State = GAME_ACTIVE;

	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;

	while( !glfwWindowShouldClose( window ) )
	{
		GLfloat currentFrame = ( float )glfwGetTime( );
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		glfwPollEvents( );
		
		BreakoutGame.ProcessInput( deltaTime );
		BreakoutGame.Update( deltaTime );

		glClearColor( 0.4f, 0.4f, 0.4f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT );
		BreakoutGame.Render( );

		glfwSwapBuffers( window );
	}
	cResourceManager::Clear( );

	glfwTerminate( );
	return 0;
}


void key_callback( GLFWwindow* window, int key, int scancode, int action, int mode )
{
	if( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS )
		glfwSetWindowShouldClose( window, GL_TRUE );
	if( key >= 0 && key < 1024 )
	{
		if( action == GLFW_PRESS )
			BreakoutGame.Keys[ key ] = GL_TRUE;
		else if( action == GLFW_RELEASE )
			BreakoutGame.Keys[ key ] = GL_FALSE;
	}
}