#include "cResourceManager.h"

#include <iostream>
#include <sstream>
#include <fstream>

#include <SOIL2/SOIL2.h>

std::map< std::string, cShader > cResourceManager::Shaders;
std::map< std::string, cTexture2D > cResourceManager::Textures;


cShader cResourceManager::LoadShader( const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile, std::string name )
{
	Shaders[ name ] = loadShaderFromFile( vShaderFile, fShaderFile, gShaderFile );
	return Shaders[ name ];
}

cShader cResourceManager::GetShader( std::string name )
{
	return Shaders[ name ];
}

cTexture2D cResourceManager::LoadTexture( const GLchar* file, GLboolean alpha, std::string name )
{
	Textures[ name ] = loadTextureFromFile( file, alpha );
	return Textures[ name ];
}

cTexture2D cResourceManager::GetTexture( std::string name )
{
	return Textures[ name ];
}

void cResourceManager::Clear( )
{
	for( auto iter : Shaders )
		glDeleteProgram( iter.second.ID );
	for( auto iter : Textures )
		glDeleteTextures( 1, &iter.second.ID );
}

cShader cResourceManager::loadShaderFromFile( const GLchar* vShaderFile, const GLchar* fShaderFile, const GLchar* gShaderFile /*= nullptr */ )
{
	std::string vertexCode;
	std::string fragmentCode;
	std::string geometryCode;
	try
	{
		std::ifstream vertexShaderFile( vShaderFile );
		std::ifstream fragmentShaderFile( fShaderFile );
		std::stringstream vShaderStream, fShaderStream;

		vShaderStream << vertexShaderFile.rdbuf( );
		fShaderStream << fragmentShaderFile.rdbuf( );

		vertexShaderFile.close( );
		fragmentShaderFile.close( );

		vertexCode = vShaderStream.str( );
		fragmentCode = fShaderStream.str( );

		if( gShaderFile != nullptr )
		{
			std::ifstream geometryShaderFile( gShaderFile );
			std::stringstream gShaderStream;
			gShaderStream << geometryShaderFile.rdbuf( );
			geometryShaderFile.close( );
			geometryCode = gShaderStream.str( );
		}
	}
	catch( std::exception e )
	{
		std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
	}
	const GLchar *vShaderCode = vertexCode.c_str( );
	const GLchar *fShaderCode = fragmentCode.c_str( );
	const GLchar *gShaderCode = geometryCode.c_str( );

	cShader Shader;
	Shader.Compile( vShaderCode, fShaderCode, gShaderFile != nullptr ? gShaderCode : nullptr );
	return Shader;
}

cTexture2D cResourceManager::loadTextureFromFile( const GLchar* file, GLboolean alpha )
{
	cTexture2D Texture2D;
	if( alpha )
	{
		Texture2D.InternalFormat = GL_RGBA;
		Texture2D.ImageFormat = GL_RGBA;
	}

	int Width, Height;
	
	unsigned char* image = SOIL_load_image( file, &Width, &Height, 0, Texture2D.ImageFormat == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB );
	const char* error = SOIL_last_result( );
	Texture2D.Generate( Width, Height, image );

	SOIL_free_image_data( image );
	return Texture2D;
}

