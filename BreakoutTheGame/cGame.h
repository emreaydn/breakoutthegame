#ifndef CGAME
#define CGAME

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <vector>
#include "cGameLevel.h"

enum GameState
{
	GAME_ACTIVE,
	GAME_MENU,
	GAME_WIN
};

class cGame
{
public:
	GameState State;
	GLboolean Keys[ 1024 ];
	GLuint Width, Height;
	std::vector< cGameLevel > Levels;
	GLuint Level;

	cGame( GLuint Width, GLuint Height );
	~cGame( );

	void Init( );
	void ProcessInput( GLfloat dt );
	void Update( GLfloat dt );
	void Render( );
};
#endif // !CGAME
