#ifndef CTEXTURE2D
#define CTEXTURE2D

#include <glad/glad.h>

class cTexture2D
{
public:
	GLuint ID;
	GLuint Width, Height; // Texture Image Dimensions
	GLuint InternalFormat; // Format of the texture object
	GLuint ImageFormat; // Format of the loaded image
	int* imageChannel;
	GLuint WrapS, WrapT;
	GLuint FilterMin, FilterMax;

	cTexture2D( );

	void Generate( GLuint Width, GLuint Height, unsigned char* data );

	void Bind( ) const;
};
#endif
