#include "cGameObject.h"

cGameObject::cGameObject( ) : Sprite( )
{
	this->Position = glm::vec2( 0.0f );
	this->Size = glm::vec2( 1.0f );
	this->Velocity = glm::vec2( 0.0f );
	this->Color = glm::vec3( 1.0f );
	this->Rotation = 0.0f;

	this->IsSolid = false;
	this->Destroyed = false;
	return;
}

cGameObject::cGameObject( glm::vec2 pos, glm::vec2 size, cTexture2D sprite, glm::vec3 color /*= glm::vec3( 1.0f )*/, glm::vec2 velocity /*= glm::vec2( 0.0f, 0.0f ) */ )
{
	this->Position = pos;
	this->Size = size;
	this->Velocity = velocity;
	this->Color = color;
	this->Rotation = 0.0f;

	this->Sprite = sprite;

	this->IsSolid = false;
	this->Destroyed = false;
	return;
}

void cGameObject::Draw( cSpriteRenderer &renderer )
{
	renderer.DrawSprite( this->Sprite, this->Position, this->Size, this->Rotation, this->Color );
}
